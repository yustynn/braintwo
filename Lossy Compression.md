---
tags: concept
created: 211024
---

Throwing away information while attempting to retain the most important parts of some data.

# Strategies
1. Throw away some data points
2. Throw away some dimensions

Similar to [[Dimensionality Reduction]]