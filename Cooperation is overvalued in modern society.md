---
tags: scratch wip n
created: 211028
---
[[Cooperation]] is overvalued in modern society.

[[Evolution]] took place in an extremely local community, and [[Evolutioary Traits are lagging indicators of what works in a dynamic enviroment]] and modern society places less importance on [[Repeated Games]].

# Reason
Why: [[Reciprocal Altruism]] made sense in small local communities where long-term [[Trust]] relationships was important for [[Goodwill]]. 

However, the amount of [[Goodwill]] gained through reciprocity and lost through non-reciprocity isn't so clear cut now because relationships are more fleeting and global.
