---
tags: concept
created: 211028
---

A [[Neural Notes|Neural Note]] is the base content unit of my note taking system.


# Properties
- [[Neural Notes are composable modules]]
- [[Neural Note titles are APIs, the contents are black boxes]]
- [[Examples should be in all neural notes that can have them]]

# Influences
- Andy Matuschak's [[Evergreen Notes]]
- Luhmann's [[Zettelkasten]]

# Open Questions
- Are index notes [[Neural Notes]]?
- Are concept notes [[Neural Notes]]?