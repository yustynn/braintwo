---
tags: wip n concept
created: 211027
---

Occurs when a query brings too much [[Cognitive Load]] to compute. Our brains substitute it with a more manageable query without us even realizing it.

# Examples
- The generally, computationally intensive "How are things?" can get auto-translated into the more manageable, specific "Are you happy in your love life?"

# References
[Wikipedia](https://en.wikipedia.org/wiki/Attribute_substitution)