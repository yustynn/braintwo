---
tags: link
created: 211018
---
[[Hedgehog's Dilemma]] $\leftrightarrow$ [[Goldilocks Problem]]

# Similarities
- Both are [[Balance Problem]]s
- Both use binary views
	- [[Hedgehog's Dilemma]]: Pain and warmth
	- [[Goldilocks Problem]]: Too much or too little
- Both are about maximizing a particular objective

# Differences
- [[Goldilocks Problem]] is optimally solvable, [[Hedgehog's Dilemma]] isn't.
	- The [[Hedgehog's Dilemma]]-type problem cannot reach the optimal solution due to the constraint of avoiding pain
	- [[Goldilocks Problem]] can reach optimal solution (just have the right balance)