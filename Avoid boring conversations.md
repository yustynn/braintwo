---
tags: n weapon
---

Avoid boring conversations. 

# Weapon Categories
## Change my own viewpoint to find the conversation interesting
- [[Start conversations by asking people interesting questions]]

## Change the boring conversation topics to something interesting
## End the conversation


# Why?
- I get negative value (bad mood, detachment) from boring conversations, and positive value (good mood, ideas) from interesting conversations
- Boring conversations with Person A lead to more boring conversations with Person A because [[Conversation is a repeated game with reputational traps]]

# What is boring?
Subjectively defined as *that which bores me*.


# Pros
