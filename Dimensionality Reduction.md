---
tags: concept
created: 211028
---

Reduce the number of dimensions that you're looking at, in order to make operations on some data more computationally tractable.

# [[Dimensionality Reduction]] $\subset$ [[Lossy Compression]] (strictly)
[[Dimensionality Reduction]] cannot throw away data points but [[Lossy Compression]] can.
