---
tags: n weapon
created: 211031
---

Start conversations by asking people interesting questions.

This helps [[Avoid boring conversations]].


# Why does this work?
It biases the entire conversation to be interesting by setting good interaction foundations, even for future topics.

I suspect it's because [[Conversation is a repeated game with reputational traps]]. Getting a good initial game (first convo topic) creates a virtuous cycle.

# References
- [1] Conversation with Anselm De Mello on 211030 went great. I started it by asking him about [[Classify people by the order of effects they're primarily interested in.]]. I think that was the main cause of the interesting overall conversation.