---
tags: n
created: 211031
---

Conversation is a repeated game with reputational traps

This operates on the following levels:
1. Individual conversation session with Person A
2. All historical conversations with Person A (though weighted by recency)
