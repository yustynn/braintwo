---
tags: n
created: 211024
---
[[Economic Value]] is distinct from [[Virtuous Value]].

[[Economic Value]] is defined in terms of money. [[Virtuous Value]] (my own term) is abuot that which helps in some moral goal (self-actualization, preserving humanity in the long run, etc).

# Examples of Unvirtuous Economic Value
- Facebook has a market cap of USD890B (as of 211027). Highly disproportionate to the actual value it brings to preserving the human species.
- Gambling rings are vice-based and economically lucrative.
- Financial derivatives are financially lucrative without having concrete virtue.