---
tags: concept
---

Getting a new option shouldn't make you pick something you initially rejected.

# Example
In **Universe A**, you're offered an *Apple* or a *Banana*. You choose an *Apple*.

In **Universe B,** you're offered an *Apple*, a *Banana* or a *Cherry.*
- You choose a *Banana* (which is neither your choice in **Universe A,** nor the new choice *Cherry* in **Universe B**).

Congratulations jackass, you just violated the [[Independence of Irrelevant Alternatives]].

## Related
[[Social Welfare Function]]