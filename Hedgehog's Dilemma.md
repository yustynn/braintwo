---
tags: concept
created: 211018
---

Metaphor for how human intimacy is a binary [[Balance Problem]].

Too much intimacy $\rightarrow$ mutual self harm

Too little intimacy $\rightarrow$ feelings of emptiness.

# The Metaphor
- Hedgehogs are cold during winter
-  Hedgehogs can get warm by huddling together
-  If hedgehogs get too close, then they hurt each other with their quills

Thus to avoid pain, they eventually settle on some middle ground which is not optimal for warmth maximization.

# Concept Origin and Elaboration
Originated from Schopenhauer (according to wiki). Excerpt:
>One cold winter's day, a number of porcupines huddled together quite closely in order through their mutual warmth to prevent themselves from being frozen. But they soon felt the effect of their quills on one another, which made them again move apart. Now when the need for warmth once more brought them together, the drawback of the quills was repeated so that they were tossed between two evils, until they had discovered the proper distance from which they could best tolerate one another. Thus the need for society which springs from the emptiness and monotony of men's lives, drives them together; but their many unpleasant and repulsive qualities and insufferable drawbacks once more drive them apart. The mean distance which they finally discover, and which enables them to endure being together, is politeness and good manners. Whoever does not keep to this, is told in England to 'keep his distance.' By virtue thereof, it is true that the need for mutual warmth will be only imperfectly satisfied, but on the other hand, the prick of the quills will not be felt. Yet whoever has a great deal of internal warmth of his own will prefer to keep away from society in order to avoid giving or receiving trouble or annoyance.


# References
- [Wikipedia](https://en.wikipedia.org/wiki/Hedgehog%27s_dilemma)

# Related
[[Hedgehog's Dilemma VS Goldilocks Problem]]