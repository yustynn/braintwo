---
tags: n
created: 211024
---
[[Categorization]] can induce the wrong conclusion.

This is because [[Categorization is sometimes done through lossy compression]]. After the information loss, things that aren't actually nails can look just right for your hammer.

# Examples
## Lobotomies
>  "Normal psychic life depends upon the good functioning of brain synapses, and mental disorders appear as a result of synaptic derangements. Synaptic adjustments will then modify the corresponding ideas and force them into different channels. Using this approach we obtain cures and improvements but no failures." - *Egas Moniz*

[source](https://www.robertsapolskyrocks.com/intro-to-human-behavioral-biology.html)

- Thought Category: Brain function can be explained (solely?) by synapses.
- This last sentence is demonstrably false. Moniz likely disregarded other viewpoints
	- See [effects](https://en.wikipedia.org/wiki/Lobotomy#Effects).
- Note: Moniz won a nobel prize for this.

# References
- Human Behavioral Biology course by Robert Sapolsky - [Lecture 1: Introduction to Human Behavioral Biology](https://www.youtube.com/watch?v=NNnIGh9g6fA)