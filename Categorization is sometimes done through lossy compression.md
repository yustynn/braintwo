---
tags: wip n
created: 211024
---
[[Categorization]] is sometimes done through [[Lossy Compression]].

Most categories are  [[Lossy Compression]]. Note that this is not about the category itself, but rather the process of [[Categorization]].

# Information Loss, or what're you looking at?
If you look at the entireity of the thing, then there's no information loss. 

However, if you throw away some features, then there's information loss. When cooking a meal, I won't use every ingredient in the fridge but only the relevant ones.

# Examples
Each datapoint X has fields a, b.

| a | b | Score|
|--|--|--|
|  0 | 0 | 0|
| 1 | 0 | 1 |
| 0 | 1 | 1 |
| 1 | 1 | 2|

Score function: $s(a, b) = a + b$
Buckets
[6, 10]: A
[0, 5]: B

## Lossless: Score Components $\rightarrow$ Aggregated Score
****
1. Run a function over (a, b) <-- result is category

There's no intermediate step with information loss here.

## Lossy: Score -> Letter Grade
1. Throw away exact score and just save bucket <--- Loss
2. Translate bucket to letter <--- this is the category

# References
- [Wikipedia page on Malay Race](https://en.wikipedia.org/wiki/Malay_race)