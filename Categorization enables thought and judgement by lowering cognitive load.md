---
tags: n
created: 211024
---

[[Categorization]] enables thought and judgement by lowering [[Cognitive Load]].

Lots of things, especially continuums, require too much [[Cognitive Load]] for real-time processing or even offline processing. Descriptively, [[Categorization]] is something we do in order to reduce that processing cost.

Note that [[Categorization is sometimes done through lossy compression]].

We also do this with *evaluations*  (e.g. if you run a mile in <4 mins, you're a great runner).

# Examples
## Positive Examples of Real Life
- Everytime your brain processes a word from a continuum of sound. There's too much information to process.
- Everytime you categorize shapes and colors as a chair

## Negative Examples of Real Life
- Someone paints with 11 colors $\rightarrow$  they're a good painter.
	- Nobody thinks this.
	
# Issues
- [[Categorizations can be misleading because boundaries are arbitrary]]
- [[Categorizations can induce the wrong conclusion]]


# Related
- Similar to [[Attribute Substitution]]. Both decrease [[Cognitive Load]].

# References
- Human Behavioral Biology course by Robert Sapolsky - [Lecture 1: Introduction to Human Behavioral Biology](https://www.youtube.com/watch?v=NNnIGh9g6fA)