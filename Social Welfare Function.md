---
tags: concept
---

A function that establishes a weak total ordering of all social states.

Put another way, it determines society's ranking of states.

# Elaboration
Put another way, it allows you to determine exactly one of the following three evaluations for each possible pair of social states:
- more desirable
- less desirable
- indifferent.

# Possible Properties
[[Independence of Irrelevant Alternatives]]
[[Unanimity]]