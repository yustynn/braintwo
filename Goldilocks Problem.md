---
tags: concept
created: 211018
---

A [[Balance Problem]] where overshooting and undershooting are both bad, possibly for different reasons.

They are optimally solvable.

Note: "Goldilocks Problem" is my own term.

# Related
[[Hedgehog's Dilemma VS Goldilocks Problem]]