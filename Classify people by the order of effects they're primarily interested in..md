---
tags: n weapon
created: 211031
---

Classify people by the order of effects they're primarily interested in. [1]

# Order of Effects
| Order | Description | Example |
|---|---|---|
| 0 | The thing itself | Making a game with the theme of loss |
| 1 | The effects on those it touches directly | Players explore theme of loss |
| 2 | The effects caused by 1st-order effects | Society becomes better equipped to deal with loss |

# Examples
| Person | Classification |
|---|---|
| Gabriel Wong | 0th Order |
| Christian Coffrant | 2nd Order |
| Sakshi Udeshi | 2nd Order |
| Me | 0th Order (50%) and 2nd Order (50%) |

# Open Questions
- If 2nd order effects are system effects, what are 3rd order effects? Is it inter-system effects? Are there higher-order systems?
- Is the difference between 0th-order and 1st-order just delayed feedback? [2]

# References
[1] My conversation with Christian Coffrant on 211029.  It's his idea, but I'm adapting language for 0th order
[2] My conversation with Anselm De Mello on 211030. He was telling me about how he loves playing games, and how he likes pushing for longer-term objectives in his machining business