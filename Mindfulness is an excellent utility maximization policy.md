---
tags: n weapon
created: 211018
---

Even when the thing is lame, [[Mindfulness]] should be employed.

# Why
1. Trains focus, and training is better when the situation is noninteresting. Possibly related to dopamine depravation
2. It's hard to tell what's useful and what's not while in the moment.
3. It is perhaps the best way to own my life

# Notes
- Mindfulness might not be the right term.
	- Possibly "presence" instead.
	- I simply mean singular focus on what's at hand, without attention-splitting.