---
tags: index
created: 211018
---

# List
- [[Mindfulness is an excellent utility maximization policy]]
- [[Cooperation is overvalued in modern society]]
- [[Avoid boring conversations]]
- [[Act to maximize the propensity of my ideal environment]]