---
tags: concept
---

Social agreement when there's full consensus.

If everyone preferes $x$ to $y$, then society prefers $x$ to $y$.

# Elaboration
If for preference configuration $R$, all participants rank $A \gt B$ then $F(R)$ ranks $A \gt B$.

$F$ is a [[Social Welfare Function | Social Welfare Function]]

# Related
[[Social Welfare Function]]