---
tags: n
created: 211024
---

[[Categorization]]s can be misleading because boundaries are arbitrary

There's often no good reason why the boundary isn't one unit higher or lower.

# Examples
## Passing an Exam vs Failing an Exam
- Exam passing mark: 50
- Student A's score: 50
- Student B's score: 49

Is Student A truly categorically better than Student B?

## Age of Consent
- Age of consent is meant to be a measure of sexual and psychological maturity
- Age of consent differs across states and countries

It's unclear which the "correct" consensual age bucket is.

# References
- Human Behavioral Biology course by Robert Sapolsky - [Lecture 1: Introduction to Human Behavioral Biology](https://www.youtube.com/watch?v=NNnIGh9g6fA)